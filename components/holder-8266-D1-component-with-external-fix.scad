use <./holder-8266-D1-component.scad>
use <./../pieces/throws-holder-external-fix.scad>

holder8266D1ComponentWithExternalFix();

/**
 * holder8266D1Component
 * @name holder8266D1Component
 * @description Full 8266 facade and holder
 * @type component
 */
 module holder8266D1ComponentWithExternalFix() {

    holder8266D1Component();

    translate([0, 4, 0])
        rotate([90,0,0])
            color("grey", alpha= 0.8)
            difference() {
                translate([0, 0, 2.5])
                    cube([80, 40, 3], center = true);

                // Todo implement external fix
                translate([0,0, 1-0.01])
                    throwsHolderExternalFix();
            }

}
