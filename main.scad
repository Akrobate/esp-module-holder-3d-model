use <./components/holder-8266-D1-component.scad>
use <./components/holder-8266-D1-component-custom-facade.scad>
use <./components/holder-8266-D1-component-with-external-fix.scad>
use <./components/holder-8266-D1-component-with-internal-fix.scad>


translate([0,0,50])
    holder8266D1ComponentCustomFacade();

holder8266D1Component();

translate([-100,0,0])
    holder8266D1ComponentWithExternalFix();

translate([100,0,0])
    holder8266D1ComponentWithInternalFix();
