#!/bin/bash


declare -g PIECES=(
facade-holder-8266-D1-piece.png
fixation-holder-8266-D1-piece.png
throws-holder-external-fix.png
throws-holder-internal-fix.png
)

# Model generation
for piece in "${PIECES[@]}"; do
    echo "Pièce : $piece"
    openscad -o render_files/$piece.png pieces/$piece.scad
done

# Components Image generation
openscad -o render_files/holder-8266-D1-component.png components/holder-8266-D1-component.scad
openscad -o render_files/holder-8266-D1-component-with-external-fix.png components/holder-8266-D1-component-with-external-fix.scad
openscad -o render_files/holder-8266-D1-component-with-internal-fix.png components/holder-8266-D1-component-with-internal-fix.scad
openscad -o render_files/holder-8266-D1-component-custom-facade.png components/holder-8266-D1-component-custom-facade.scad
openscad --camera=150,150,100,0,0,0 -o render_files/holder-8266-D1-component-custom-facade-rotated.png components/holder-8266-D1-component-custom-facade.scad

# Assets preview renders
openscad -o assets/preview/facade-holder-8266-D1-piece.png pieces/facade-holder-8266-D1-piece.scad
openscad -o assets/preview/fixation-holder-8266-D1-piece.png pieces/fixation-holder-8266-D1-piece.scad