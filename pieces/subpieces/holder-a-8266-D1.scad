include <../../configurations/global.scad>

holderA8826D1SubPiece();


/**
 * holderA8826D1SubPiece
 * @name holderA8826D1SubPiece
 * @description One of two holder part
 * @type subPiece
 */
module holderA8826D1SubPiece(
    hole_3mm_correction = holder_a_hole_3mm_correction,
    encaps_width_correction = holder_a_encaps_width_correction,

    part_holders_width = holder_a_part_holders_width,
    board_width = board_width,
    encaps_width = encaps_width,
    encaps_height = encaps_height,
    depth = holder_a_depth,
    support_depth = holder_a_support_depth,

    height = holder_a_height,
    throws_margin = holder_a_throws_margin,
    throws_diameter = holder_a_throws_diameter,

    $fn = fn_holder_a
) {

    width = board_width + (part_holders_width * 2);
    throws_diameter_with_correction = throws_diameter + hole_3mm_correction;
    encaps_width_with_correction = encaps_width + encaps_width_correction;

    difference() {
        union() {
            translate([0, depth / 2, 0])
                cube([width, depth, height], center = true);

            translate([0, depth + (support_depth / 2), 0])
                cube([14, support_depth, height], center = true);
        }

        translate([
            - (board_width - encaps_width_with_correction),
            depth / 2,
            height - encaps_height
        ])
            cube([
                encaps_width_with_correction,
                20,
                height
            ], center = true);

        translate([width / 2 - throws_margin, depth / 2, 0])
            cylinder(h = height * 2, d = throws_diameter_with_correction, center = true, $fn = $fn);

        translate([- (width / 2) + throws_margin, depth / 2, 0])
            cylinder(h = height * 2, d = throws_diameter_with_correction, center = true, $fn = $fn);

    }
}




module _holderA8826D1SubPiece(
    hole_3mm_correction = holder_a_hole_3mm_correction,
    encaps_width_correction = holder_a_encaps_width_correction,

    part_holders_width = holder_a_part_holders_width,
    board_width = board_width,
    encaps_width = encaps_width,
    encaps_height = encaps_height,
    depth = holder_a_depth,
    support_depth = holder_a_support_depth,

    height = holder_a_height,
    throws_margin = holder_a_throws_margin,
    throws_diameter = holder_a_throws_diameter,

    $fn = fn_holder_a
) {

    width = board_width + (part_holders_width * 2);
    throws_diameter_with_correction = throws_diameter + hole_3mm_correction;
    encaps_width_with_correction = encaps_width + encaps_width_correction;

    difference() {
        union() {
            translate([0, 0, 0])
                cube([width, depth, height], center = true);

            translate([0, (depth / 2) + (support_depth / 2), 0])
                cube([14, support_depth, height], center = true);
        }

        translate([
            - (board_width - encaps_width_with_correction),
            0,
            height - encaps_height
        ])
            cube([
                encaps_width_with_correction,
                20,
                height
            ], center = true);

        translate([width / 2 - throws_margin, 0, 0])
            cylinder(h = height * 2, d = throws_diameter_with_correction, center = true, $fn = $fn);

        translate([- (width / 2) + throws_margin, 0, 0])
            cylinder(h = height * 2, d = throws_diameter_with_correction, center = true, $fn = $fn);

    }
}
