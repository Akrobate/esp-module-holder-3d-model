#!/bin/bash

declare -g PIECES=(
facade-holder-8266-D1-piece
fixation-holder-8266-D1-piece
throws-holder-external-fix
throws-holder-internal-fix
)

# Model generation
for piece in "${PIECES[@]}"; do
    echo "Pièce : $piece"
    openscad -o stl_files/$piece.stl pieces/$piece.scad
done