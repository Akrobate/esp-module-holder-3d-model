use <./holder-8266-D1-component.scad>
use <./../pieces/throws-holder-internal-fix.scad>

holder8266D1ComponentWithInternalFix();

/**
 * holder8266D1Component
 * @name holder8266D1Component
 * @description Full 8266 facade and holder
 * @type component
 */
 module holder8266D1ComponentWithInternalFix() {

    holder8266D1Component();

    translate([0, 0, 0])
        rotate([90,0,0])
            color("grey", alpha= 0.8)
                difference() {
                    translate([0, 0, 1.5 + 2])
                        cube([80, 40, 3], center = true);

                    translate([0,0, 1.5])
                        throwsHolderInternalFix();
                }
}
