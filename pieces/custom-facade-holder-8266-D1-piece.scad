use <./subpieces/holder-8266-D1-wrapper.scad>

rotate([90,0,0])
    customFacadeHolder8266D1Piece();

/**
 * customFacadeHolder8266D1Piece
 * @name customFacadeHolder8266D1Piece
 * @description Facade with board holder
 * @type piece
 * @parent holder8266D1Component
 * @stl
 */
module customFacadeHolder8266D1Piece(
    board_width = 25.6,
    encaps_width = 23.36,
    encaps_height = 1,

    usb_hole_height = 6.5,
    usb_hole_width = 12,
    usb_hole_thickness = 10,

    holder_a_hole_3mm_correction = 0.35,
    holder_a_encaps_width_correction = 0.4,
    holder_a_part_holders_width = 7,
    holder_a_depth = 6,
    holder_a_support_depth = 4.5,
    holder_a_height = 4,
    holder_a_throws_margin = 3,
    holder_a_throws_diameter = 3,

    holder_a_facade_shift_size = 0.1,

    holder_positionning_z = 0,
    
    fn_facade = 200,
    fn_holder_a = 100
) {
    Holder8266D1Wrapper(
        board_width = board_width,
        encaps_width = encaps_width,
        encaps_height = encaps_height,

        usb_hole_height = usb_hole_height,
        usb_hole_width = usb_hole_width,
        usb_hole_thickness = usb_hole_thickness,
        holder_a_hole_3mm_correction = holder_a_hole_3mm_correction,
        holder_a_encaps_width_correction = holder_a_encaps_width_correction,
        holder_a_part_holders_width = holder_a_part_holders_width,
        holder_a_depth = holder_a_depth,
        holder_a_support_depth = holder_a_support_depth,
        holder_a_height = holder_a_height,
        holder_a_throws_margin = holder_a_throws_margin,
        holder_a_throws_diameter = holder_a_throws_diameter,
        holder_a_facade_shift_size = holder_a_facade_shift_size,
        holder_positionning_z = holder_positionning_z,

        fn_holder_a = fn_holder_a
    ) {

        custom_facade_x_size = 80;
        custom_facade_y_size = 40;
        custom_facade_thickness = 4;
        custom_facade_hole_3mm_correction = 0.5;
        custom_facade_throws_diameter = 3;

        custom_x_throws_margin = 10;
        custom_y_throws_margin = 10;

        translate([
            - custom_facade_x_size / 2,
            - custom_facade_y_size / 2,
            0
        ])
            difference() {
                cube([
                    custom_facade_x_size,
                    custom_facade_y_size,
                    custom_facade_thickness,
                ]);

                translate([custom_x_throws_margin, custom_y_throws_margin, -custom_facade_thickness / 2])
                    cylinder(h = custom_facade_thickness * 2, d = custom_facade_throws_diameter + custom_facade_hole_3mm_correction, $fn = fn_facade);

                translate([custom_facade_x_size - custom_x_throws_margin, custom_y_throws_margin, -custom_facade_thickness / 2])
                    cylinder(h = custom_facade_thickness * 2, d = custom_facade_throws_diameter + custom_facade_hole_3mm_correction, $fn = fn_facade);
                
                translate([custom_facade_x_size - custom_x_throws_margin, custom_facade_y_size - custom_y_throws_margin, -custom_facade_thickness / 2])
                    cylinder(h = custom_facade_thickness * 2, d = custom_facade_throws_diameter + custom_facade_hole_3mm_correction, $fn = fn_facade);

                translate([custom_x_throws_margin, custom_facade_y_size - custom_y_throws_margin, -custom_facade_thickness / 2])
                    cylinder(h = custom_facade_thickness * 2, d = custom_facade_throws_diameter + custom_facade_hole_3mm_correction, $fn = fn_facade);
            }
    }
}
