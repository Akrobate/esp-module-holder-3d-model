
// internal params
holder_a_hole_3mm_correction = 0.35;
holder_a_encaps_width_correction = 0.4;
holder_a_part_holders_width = 7;
board_width = 25.6;
encaps_width = 23.36;
encaps_height = 1;
holder_a_depth = 6;
holder_a_support_depth = 4.5;
holder_a_height = 4;
holder_a_throws_margin = 3;
holder_a_throws_diameter = 3;

fn_holder_a = 100;
